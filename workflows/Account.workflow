<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Account_Customer_Status_update_outbound</fullName>
        <apiVersion>36.0</apiVersion>
        <endpointUrl>https://preprod-elb.belongtest.com.au/project-eve/api/services/salesforce/account/status/notification</endpointUrl>
        <fields>Customer_Status__c</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Account  Customer Status update outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Account_manual_created_outbound</fullName>
        <apiVersion>35.0</apiVersion>
        <endpointUrl>https://preprod-elb.belongtest.com.au/project-eve/api/services/salesforce/account/notification</endpointUrl>
        <fields>Id</fields>
        <fields>Lease_Transferred_from__c</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Account manual created outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Fixed_Braintree_Outbound_Message</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://preprod-elb.belongtest.com.au/project-eve/api/services/salesforce/braintreeid/notification</endpointUrl>
        <fields>Braintree_Customer_ID__c</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>belongadmin@belong.com.au</integrationUser>
        <name>Fixed Braintree Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>cooling_off_termination_outbound_message</fullName>
        <apiVersion>37.0</apiVersion>
        <description>cooling off termination outbound message</description>
        <endpointUrl>https://preprod-elb.belongtest.com.au/project-eve/services/salesforce/account/coolingOff/status/notification</endpointUrl>
        <fields>Customer_Status__c</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>cooling off termination outbound message</name>
        <protected>false</protected>
        <useDeadLetterQueue>true</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
