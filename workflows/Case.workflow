<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Asset_case_outbound</fullName>
        <apiVersion>35.0</apiVersion>
        <endpointUrl>https://preprod-elb.belongtest.com.au/project-eve/api/services/salesforce/asset/notification</endpointUrl>
        <fields>AssetId</fields>
        <fields>Asset_Case_Description__c</fields>
        <fields>Asset_Case_Reason_Code__c</fields>
        <fields>Asset_Case_Reason__c</fields>
        <fields>CaseNumber</fields>
        <fields>ContactId</fields>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <fields>Product_Code__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>devops@belong.com.au</integrationUser>
        <name>Asset case outbound</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
