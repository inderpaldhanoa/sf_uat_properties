<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Mobile_Send_Notification_to_SL</fullName>
        <apiVersion>40.0</apiVersion>
        <description>Whenever a new notification is generated for customer. This sends a notification to mobile service layer.</description>
        <endpointUrl>https://internal-test.belongtest.com.au/api/v1/notification/notification-event</endpointUrl>
        <fields>Id</fields>
        <fields>Octane_Customer_Number__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>belongapi@belong.com.au</integrationUser>
        <name>Mobile-Send Notification to SL</name>
        <protected>false</protected>
        <useDeadLetterQueue>true</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
